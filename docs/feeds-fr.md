---
title: Abonnement aux flux RSS
author: Adrien André
date: 2019-08-28
---

# Abonnement aux flux RSS

Les [flux RSS](https://fr.wikipedia.org/wiki/RSS) permettent de rester informé des actualités publiées par les sites web.
On utilise un logiciel agrégateur pour vérifier les nouveautés sur un ensemble de sites.


## Les agrégateurs

### Les clients mél

* Thunderbird ([documentation](https://support.mozilla.org/fr/kb/comment-s-abonner-aux-flux-de-nouvelles-et-blogs))
* Outlook ([documentation](https://support.office.com/en-us/article/Subscribe-to-an-RSS-Feed-73c6e717-7815-4594-98e5-81fa369e951c))


### Dans le navigateur

* L'extension [FeedBro](https://addons.mozilla.org/fr/firefox/addon/feedbroreader)
* l'outil web [FramaNews](https://framanews.org) ([TTRSS](http://tt-rss.org))
    
    
## Les flux OpenStreetMap

* Les notes : [OSM QA feeds](https://tyrasd.github.io/osm-qa-feeds)
* l'actualité [HebdOSM](http://weeklyosm.eu/fr) ([flux](http://www.weeklyosm.eu/feed?lang=fr))
* l'assurance qualité [Osmose](http://osmose.openstreetmap.fr/fr/map/#zoom=8&lat=3.669&lon=-53&item=xxxx&level=1) ([par utilisateur](http://osmose.openstreetmap.fr/byuser/nomdutilisateur.rss?level=1))
* les éditions : [WhoDidIt](https://tyrasd.github.io/osm-qa-feeds)
* les nouveaux utilisateurs : [newestosm](https://tyrasd.github.io/osm-qa-feeds)
* les données géographiques : [GéoGuyane](https://catalogue.geoguyane.fr) ([flux](https://catalogue.geoguyane.fr/rss/generate_flux_rss/NOUVEAUTE/dataset))


## S'abonner

### FeedBro
    
1. Se rendre sur [OSM QA feeds](https://tyrasd.github.io/osm-qa-feeds)
2. renseigner "Guyane" et sélectionner "French Guiana, France"
3. ajuster la zone si besion et cliquer sur Continue
4. clic droit sur _OSM notes_, _Copier l'adresse du lien_
5. cliquer sur l'icône FeedBro dans la barre Firefox et sélectionner _Open feed browser_
6. cliquer sur _Add a new feed_ (icône de flux orange avec petit + vert) et coller l'adresse dans le champ _Feed URL_
7. cliquer sur _Load_ puis _Save_
8. afficher les éléments en cliquant sur _All items_
9. répéter les opérations pour les URLs de flux trouvées sur des sites d'intérêt (HebdOSM, Osmose)

### Thunderbird
    
1. Trouver l'URL du flux
2. dans thunderbird, clic droit sur la catégorie _Blogs et nouvelles_, sélectionner _S'abonner…_
3. dans le champ _Adresse du flux_, coller l'URL
4. cliquer sur _Ajouter_ puis _Fermer_. Le flux a été ajouté dans la catégorie _Blogs et nouvelles_


## Trouver les flux

* Recherche manuelle sur chaque site
* l'extension Firefox [Want My RSS](https://addons.mozilla.org/fr/firefox/addon/want-my-rss)



Ce document est partagé sous licence [Licence Creative Commons BY-SA](http://creativecommons.org/licenses/by-sa/3.0/fr) 4.0 International
