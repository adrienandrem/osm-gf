---
title: OpenStreetMap & OSM-GF
author: Adrien André
date: 2019-09-28
---

OpenStreeMap
============

## Base de donnée

Base de donnée sous licence ouverte (ODbL)

1. Modifier
2. partager
3. réutiliser


## Carte

![www.openstreetmap.org](https://wiki.openstreetmap.org/w/images/thumb/8/80/Browsing.png/594px-Browsing.png)


## Contribuer

* Signaler une amélioration possible par une [note](https://wiki.openstreetmap.org/wiki/FR:Notes)  
![Ajouter une note](https://wiki.openstreetmap.org/w/images/8/8b/Add_a_note_screenshot.png)
* iD : l'éditeur en ligne
* éditions avancées avec JOSM
* documentation sur le Wiki [wiki.openstreetmap.org](http://wiki.openstreetmap.org)


## La communauté

* Association France OSM-Fr [www.openstreetmap.fr](https://www.openstreetmap.fr)
* liste de discussion [talk-fr](https://lists.openstreetmap.org/listinfo/talk-fr)
* forum d'aide [forum.openstreetmap.fr](https://forum.openstreetmap.fr)
* Twitter : @OSM\_FR
* Mastodon : @osm\_fr


# OpenStreetMap Guyane

## L'association

* OpenStreetMap Guyane
* Activité :
    - Faire connaître
    - accompagner les utilisateurs.
* Rencontres mensuelles  
le dernier mardi du mois
* Ateliers


## Sujets

* Structures de santé
* Occupation du sol
* Hydrographie et toponymie
* Contrôle erreurs


## Contact

* Courriel [osm-gf@laposte.net](mailto:osm-gf@laposte.net)
* Liste de discussion  
[local-guyane@listes.openstreetmap.fr](mailto:local-guyane@listes.openstreetmap.fr)
* Mastodon @osm\_gf



----
Ce document est partagé sous licence [Licence Creative Commons BY-SA](http://creativecommons.org/licenses/by-sa/3.0/fr) 4.0 International
