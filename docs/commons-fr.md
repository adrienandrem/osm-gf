---
title: Communs
author: Adrien André
date: 2019-10-15
---

# Description

[Wikipédia](https://fr.wikipedia.org) contient des articles géoréférencés (exemple : [Zoo de Guyane](https://fr.wikipedia.org/wiki/fr:Zoo%20de%20Guyane)).

[Wikimédia](https://commons.wikimedia.org) contient des fichiers dont certains géoréférencés,
comme la [photo de la mairie de Cayenne](https://commons.wikimedia.org/wiki/File:Mairiecayenne.jpg).

[Wikidata](https://www.wikidata.org) est une base de connaissance structurée en concepts et liens.

Créer des liens entre les éléments OSM et les articles Wikipédia permet, par exemple,
d'accéder aux articles et photos des éléments remarquables, directement depuis la carte.

On peut lier un élément OSM à son article Wikipédia grâce à l'attribut [wikipedia=*](https://wiki.openstreetmap.org/wiki/FR:Key:wikipedia).
Le lien de avec l'élément Wikidata se fait avec l'attribut  [wikidata=*](https://wiki.openstreetmap.org/wiki/FR:Key:wikidata).


## Outils

* Contenu Wikipédia à propos de ma commune : [Ma commune Wikipédia](https://macommune.wikipedia.fr/Q44401/Cayenne)
* Affichage des articles, photos, etc géoréférencés : [WikiMap](https://tools.wmflabs.org/wikimap/?lon=-52.3253&lat=4.8488&wp=false&zoom=14) (amélioration de [WiWOSM](tools.wmflabs.org/wiwosm/osm-on-ol/commons-on-osm.php?zoom=14&lon=-52.3239&lat=4.9178&layers=B00TFT))
* Trouver les liens potentiellement à ajouter : [OSM-Wikidata matcher](https://osm.wikidata.link)
* Carte de la couverture des photos Wikimédia géoréférencées : [Commons Coverage](https://tools.wmflabs.org/commons-coverage/#14/4.9178/-52.3239)

* Comparaison OSM-Wikidata : [Sophox](https://wiki.openstreetmap.org/wiki/Sophox)


---

Ce document est partagé sous licence [Licence Creative Commons BY-SA](http://creativecommons.org/licenses/by-sa/3.0/fr) 4.0 International
