---
title: Langages
author: Adrien André
date: 2016-09-22
---

# Langages

langage                                                                 | ISO 639-2 code                                | attribut
----------------------------------------------------------------------- | --------------------------------------------- | ----------------------------------------------------------
[Teko](http://en.wikipedia.org/wiki/fr:%C3%89m%C3%A9rillon) (Émérillon) | [eme](https://iso639-3.sil.org/code/eme) | [name:eme](https://wiki.openstreetmap.org/wiki/Key:name)=*
[Wayampi](http://en.wikipedia.org/wiki/fr:Way%C3%A3pi)                  | [oym](https://iso639-3.sil.org/code/oym) | [name:oym](https://wiki.openstreetmap.org/wiki/Key:name)=*
[Wayana](http://en.wikipedia.org/wiki/fr:Wayana)                        | [way](https://iso639-3.sil.org/code/way) | [name:way](https://wiki.openstreetmap.org/wiki/Key:name)=*
[Aluku](http://en.wikipedia.org/wiki/fr:Aluku)                          | [djk](https://iso639-3.sil.org/code/djk) | [name:djk](https://wiki.openstreetmap.org/wiki/Key:name)=*



Ce document est partagé sous licence [Licence Creative Commons BY-SA](http://creativecommons.org/licenses/by-sa/3.0/fr) 4.0 International
