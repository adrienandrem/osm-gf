---
title:  Comment améliorer la carte ?
author: Adrien André
date:   2019-03-13
---

Comment améliorer la carte ?
============================

Ce document vise à orienter le contributeur désirant enrichir la carte et cherchant par où commencer.

# 1. Zones de vie

Repérer les zones de vie principales et tracer les _zones résidentielles_ : [landuse=residential][residential]
    
# 2. Réseau

Tracer les voies qui relient ces zones de vie :

1. voies terrestres : [highway=*][way] ([exemples sur le wiki][wayExample]) ;
2. voies navigables : [waterway=*][waterway].
    
# 3. Toponymie

Nommer les zones de vie et les cours d'eau
        
# 4. Points d'intérêt

Ajouter les points d'intérêt principaux.  
N.B. : **Placer un simple noeud est suffisant**.

* le _poste de police_ : [amenity=police][police] ([exemple à Sinnamary][policeEx])
* la _caserne de pompiers_ : [amenity=fire_station][fire] ([exemple à Cacao][fireEx])
* _cabinet médical_ et _pharmacie_ : [amenity=doctors][medecine] et [amenity=pharmacy][pharmacy] (exemples : [médecin de Roura][medecineEx] et [pharmacie de Maripasoula][pharmacyEx])
* l'_école_ : [amenity=school][school] ([exemple à Régina][schoolEx])
* la _place du marché_ : [amenity=marketplace][market] ([exemple à Sinnamary][marketEx])
* l'_église_ : [amenity=place_of_worship][eglise] ([exemple à Cacao][egliseEx])
* le _bureau de poste_ : [amenity=post_office][post] ([exemple à Awala][postEx])
* le _carbet communal_ : [tourism=chalet][carbetCom] ([exemple à Grand-Santi][carbetComEx])
   
Éventuellement, tracer les bâtiments qui les accueillent.
    
# 5. Bâtiments isolés

Tracer les bâtiments isolés.
        
# 6. Villages

Au sein des zones de vie :

* tracer les bâtiments ;
* tracer les voies principales.
        
# 6. Sur le terrain

1. Résoudre des [notes][notes] ;
2. chasser les [erreurs et améliorations possibles][qa] ;
3. ajouter les noms manquants : [name=*][name].

---

OpenStreetMap Guyane  
Mars 2019  
Document sous licence [Licence Creative
Commons BY-SA (Attribution - Partage dans les Mêmes
Conditions) 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/deed.fr).



[residential]: https://wiki.openstreetmap.org/wiki/FR:Tag:landuse=residential
[way]: https://wiki.openstreetmap.org/wiki/FR:Voirie
[wayExample]: https://wiki.openstreetmap.org/wiki/Highway_tagging_samples/out_of_town
[waterway]: https://wiki.openstreetmap.org/wiki/FR:Cours_d%27eau
[police]: https://wiki.openstreetmap.org/wiki/FR:Tag:amenity%3Dpolice
[fire]: https://wiki.openstreetmap.org/wiki/FR:Tag:amenity%3Dfire_station
[medecine]: https://wiki.openstreetmap.org/wiki/FR:Tag:amenity%3Ddoctors
[pharmacy]: https://wiki.openstreetmap.org/wiki/FR:Tag:amenity%3Dpharmacy
[school]: https://wiki.openstreetmap.org/wiki/FR:Tag:amenity%3Dschool
[market]: https://wiki.openstreetmap.org/wiki/FR:Tag:amenity%3Dmarketplace
[eglise]: https://wiki.openstreetmap.org/wiki/FR:Tag:amenity%3Dplace_of_worship
[post]: https://wiki.openstreetmap.org/wiki/FR:Tag:amenity%3Dpost_office
[carbetCom]: https://wiki.openstreetmap.org/wiki/FR:Tag:tourism=chalet
[notes]: https://www.openstreetmap.org/#map=9/4.9706/-52.9211&layers=N
[qa]: http://osmose.openstreetmap.fr/fr/map/#zoom=9&lat=4.97&lon=-52.922&item=5xxx%2C7xxx%2C9xxx&level=1&tags=&fixable=
[name]: https://wiki.openstreetmap.org/wiki/FR:Key:name

[residentialEx]: http://osm.org/way/622812888
[wayEx]: http://osm.org/way/34838381
[waterwayEx]: http://osm.org/way/340449592
[policeEx]: http://osm.org/node/4149089223
[fireEx]: http://osm.org/node/5127407905
[medecineEx]: http://osm.org/node/2516651072
[pharmacyEx]: http://osm.org/node/2133568292
[schoolEx]: http://osm.org/node/6185598748
[marketEx]: http://osm.org/node/2172660348
[egliseEx]: http://osm.org/node/1607730020
[postEx]: http://osm.org/node/4271715339
[carbetComEx]: http://osm.org/way/313498964
